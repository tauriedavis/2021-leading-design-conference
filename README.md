# 2021 Leading Design

This project includes notes from the talks given at the [2021 Leading Design fesitval](https://leadingdesign.com/conferences/festival-2021).

[Snippets](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets) will be created for each talk and shared publicly.

## Day one

1. [The Many Facets of Design Leadership](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2084588) - Julie Zhuo
1. [Leading Successfully, Through Leading Ourselves](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2084660) - Aaron Irizarry
1. [Designing Your Design Org](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2084681) - Kristin Skinner
1. [Sketchbooks over Spreadsheets: Designers As Leaders In A Complex World](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2084706) - Doug Powell

## Day two

1. [The Three Ages Of Leadership](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2085166) - Jane Austin
1. [Leading From The Middle, Lessons From The IC Track](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2085167) - D. Keith Robinson
1. [Design Leadership For Imposters](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2085168) - Temi Adeniyi
1. [Mostly Clear, Partly Sunny: Creating the Conditions for Design to Flourish](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2085363) - Michael Yap

## Day Three

1. [Mythbusting Millennial Management](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2085924) - Christine Pizzo
1. [The Manager's Guide To 1:1's](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2085999) - Abi Jones
1. [Design Leadership For Introverts (Iso-Edition)](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2085997) - Timothy Yeo
1. [Building The Figma Design Team](https://gitlab.com/tauriedavis/2021-leading-design-conference/-/snippets/2086099) - Noah Levin
